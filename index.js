const container = document.querySelector(".container");
const seats = document.querySelectorAll(".row .seat:not(.occupied)");
const count = document.getElementById("count");
const total = document.getElementById("total");
const movieSelect = document.getElementById("movie");
const currencySelect = document.getElementById("currency");
//variable para almacenar el valor de la moneda actual de la página
let current_currency = document.getElementById("currency").value;
//array de datos para llenar dinamicamente el select
let movies_array = {
  10: "Avengers",
  12: "Jocker",
  8: "Toy Story",
  9: "The Lion King",
  6: "Mulan",
};

//Lleno los datos del SELECT de peliculas
function populateMovieSelect(movies_array) {
  movieSelect.length = 0;
  for (index in movies_array) {
    movieSelect.options[movieSelect.options.length] = new Option(
      movies_array[index] + " (" + index + " " + current_currency + ")",
      index
    );
  }
}

populateMovieSelect(movies_array);
populateUI();
let ticketPrice = +movieSelect.value;

function setMovieData(movieIndex, moviePrice) {
  localStorage.setItem("selectedMovieIndex", movieIndex);
  localStorage.setItem("selectedMoviePrice", moviePrice);
}

function updateSelectedCount() {
  const selectedSeats = document.querySelectorAll(".row .seat.selected");
  const seatsIndex = [...selectedSeats].map((seat) => [...seats].indexOf(seat));
  localStorage.setItem("selectedSeats", JSON.stringify(seatsIndex));
  const selectedSeatsCount = selectedSeats.length;
  count.innerText = selectedSeatsCount;
  total.innerText =
    (selectedSeatsCount * ticketPrice).toFixed(2) + " " + current_currency;
  //total.innerText = current_currency;
}

//get data from local storage
function populateUI() {
  const selectedSeats = JSON.parse(localStorage.getItem("selectedSeats"));
  if (selectedSeats !== null && selectedSeats.length > 0) {
    seats.forEach((seat, index) => {
      if (selectedSeats.indexOf(index) > -1) {
        seat.classList.add("selected");
      }
    });
  }

  const selectedMovieIndex = localStorage.getItem("selectedMovieIndex");
  if (selectedMovieIndex !== null) {
    movieSelect.selectedIndex = selectedMovieIndex;
  }
}

function calculate(new_currency, index_movie_select) {
  fetch(`https://api.exchangerate-api.com/v4/latest/${current_currency}`)
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      const rate = data.rates[new_currency];
      current_currency = new_currency;
      let new_movie_array = {};
      for (index in movies_array) {
        new_movie_array[(index * rate).toFixed(2)] = movies_array[index];
      }
      movies_array = new_movie_array;
      //Lleno el SELECT de las peliculas con los precios convertidos a la moneda seleccionada
      populateMovieSelect(new_movie_array);
      movieSelect.selectedIndex = index_movie_select;
      //actualizo el valor del precio por el nuevo valor seleccionado en el SELECT de las películas
      ticketPrice = +movieSelect.value;
      //Actualizo los asientos seleccionados para el calculo del precio a pagar por el nuevo valor de la moneda seleccionada
      updateSelectedCount();
    })
    .catch(function (err) {
      console.log(err.status);
    });
}

currencySelect.addEventListener("change", (e) => {
  calculate(e.target.value, movieSelect.selectedIndex);
});

movieSelect.addEventListener("change", (e) => {
  ticketPrice = +e.target.value;
  setMovieData(e.target.selectedIndex, e.target.value);
  updateSelectedCount();
});

container.addEventListener("click", (e) => {
  if (
    e.target.classList.contains("seat") &&
    !e.target.classList.contains("occupied")
  ) {
    e.target.classList.toggle("selected");
    updateSelectedCount();
  }
});

updateSelectedCount();
